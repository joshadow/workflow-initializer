<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\GridSoftexpert;

class Workflow extends Controller
{
    use GridSoftexpert;
    public function newChildEntityRecord(Request $request)
    {
        $entity = $request->entity;
        $recordid = $request->recordid;
        $relationship = $request->relationship;
    }
    public function newWorkflowAndUpdateData(Request $request)
    {
         //ID del Proceso que se quiere instanciar
         $processId = "APAP01";
     
         //Nombre de la tabla en Softexpert
         $entity = 'APAPSolServ';
         $entityCall = [];
 
         ///////////////////////IS THE ARRAY ABOUT DATA FORM RETRIEVED/////////////////////////////////
         $data = $request->data;
       
         // /dd($data);
 
         //Connecting to webservice
        $url = "https://13.59.14.158/softexpert/webserviceproxy/se/ws/wf_ws.php";
        
        $wf = new \nusoap_client($url, false);//Esta url es la de la webservice de Workflow, se captura desde un archivo de configuracion (.env)
        $wf->setCredentials('apap01', '111111');  

        $wf->soap_defencoding = 'UTF-8';
        $wf->decode_utf8 = false;
         //Se refiere a un usuario y contraseña validos en Softexpert, el cual debe tener permisos para instanciar todos los procesos.
     
         //collecting the data from the form
      // dd($data);
       
         //Lista de atributos que se quieren modificar en la tabla
         $EntityAttributeList['EntityAttribute'] = array();
         for ($i = 0; $i < count($data); $i++) {
             $EntityAttribute = array('EntityAttributeID' => $data[$i]['key'], 'EntityAttributeValue' => utf8_decode($data[$i]['value']));
             array_push($EntityAttributeList['EntityAttribute'], $EntityAttribute);
         }
 
         //dd($EntityAttributeList);
         //Construccion de los parametros de la entidad
         $entityCall['Entity'] = array('EntityID' => $entity, 'EntityAttributeList' => $EntityAttributeList);
       
         //Configuracion de los parametro de la llamada del metodo
         $callParams = ['ProcessID' => $processId, 'WorkflowTitle' => 'Titulo de Prueba Workflow', 'UserID' => 'apap01', 'EntityList' => $entityCall];
         //d($callParams);
         //Instancia Workflow y edita los atributos de la tabla
         $response = $wf->call('newWorkflowEditData', $callParams);
        
         //Si es falso hubo un error de configuracion, normalmente por credencialñ
         if($response == false)
         {
             return ['Status' => 'error' ,'error' => 'Error de Configuracion de la llamada','detail' =>'Revisar las credenciales o url de la web service',500];
         
         }
         //WHEN SOFTEXPERT RETURNS 
         if($response['Status'] == 'FAILURE')
         {
             return ['Status' => 'error' ,'error' => 'Error al enviar la solicitud a la institucion','detail' =>$response['Detail'],500];
         }
        
         if(!array_key_exists('RecordID',$response)){
             return ['success'=> false, 'error'=>'Error en la comunicación','detail'=>utf8_decode($response['Detail'])];
         }

         return ['Status' => $response['Status'], 'Code' => $response['Code'], 'Detail' => utf8_decode($response['Detail']), 'RecordID' => $response['RecordID']];
    }


    
}
